#include "field.h"
#include <QPainter>

Field::Field(int index, int width, int height)
  :img(width, height), index(index), width(width), height(height)
{
  img.fill(Qt::black);
}

Field::Field(int index, const QPixmap &image)
  :img(image), index(index), width(image.width()), height(image.height())
{
  QPainter p(&img);
  p.setPen(Qt::black);
  p.drawRect(0, 0, img.width(), img.height());
  p.end();
}

int Field::number() const
{
  return index;
}

const QPixmap &Field::image() const
{
  return img;
}
