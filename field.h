#ifndef FIELD_H
#define FIELD_H
#include <QPixmap>

class Field
{
  QPixmap img;
  int index;
public:
  int width, height;
  Field(int index, int width, int height);
  Field(int index, const QPixmap &image);
  int number() const;
  const QPixmap& image() const;
};

#endif // FIELD_H
