#include "engine.h"
#include <QDirIterator>
#include <exception>
#include <algorithm>

bool Engine::check() const
{
  int prev = -1;
  for(const auto &f: fields)
    if(f.number() < prev)
      return false;
    else
      prev = f.number();
  return true;
}

void Engine::loadlevels()
{
  QDirIterator flevels(":level", QDirIterator::Subdirectories);
  while (flevels.hasNext()) {
      loadlevel(flevels.next());
    }
  type = -1;
  currentlevel = levels.empty() ? -1: 0;
}

bool Engine::loadlevel(const QString &fname)
{
  Level l(fname);
  if(!l.fullImage().isNull() && l.fullImage().width() <= MAX_SIZE_PIC && l.fullImage().height() <= MAX_SIZE_PIC) {
      levels.push_back(l);
      return true;
    }
  return false;
}

bool Engine::hasLevels() const
{
  return !levels.empty();
}

const Level &Engine::currentLevel() const
{
  if(currentlevel < 0)
    throw std::out_of_range("no levels");
  return levels[currentlevel];
}

void Engine::operator+=(int to)
{
  if(currentlevel < 0)
    return;
  currentlevel += to;
  if(currentlevel < 0)
    currentlevel = 0;
  if(currentlevel >= levels.size())
    currentlevel = levels.size() - 1;
}

void Engine::startGame(int type)
{
  this->type = type;
  fields.clear();
  const QPixmap &im = currentLevel().fullImage();
  int w = im.width()/type;
  int h = im.height()/type;
  int index = 0;
  for(int ih = 0; ih < h*type; ih+=h)
  for(int iw = 0; iw < w*type; iw+=w) {
      if(index != 0) {
          fields.push_back(Field(index, im.copy(QRect(iw, ih, w, h))));
        }
      else {
          fields.push_back(Field(index, w, h));
        }
      ++index;
    }
  stats = 0;
}

void Engine::shuffle()
{
  do {
      std::random_shuffle(fields.begin(), fields.end());
    }
  while(!oddcheck());
}

bool Engine::checkandswap(int index, int indexdif) {
  if(fields[index+indexdif].number() == 0) {
      std::swap(fields[index], fields[index+indexdif]);
      stats++;
      return true;
    }
  return false;
}

bool Engine::oddcheck() const
{
  int v = 0;
  for(auto curf = fields.cbegin(); curf != fields.cend(); ++curf) {
      for(int i = 1; i < (*curf).number(); ++i) {
          bool needinc = true;
          for(auto prevf = fields.cbegin(); prevf != curf; ++prevf) {
              if((*prevf).number() == i) {
                  needinc = false;
                  break;
                }
            }
          if(needinc)
            ++v;
        }
    }
  return (v%2) == 0;
}

void Engine::clickto(int index)
{
  int indexf = 0;
  for(; indexf < fields.size(); ++indexf)
    if(fields[indexf].number() == index)
      break;
  int x = indexf % type;
  int y = indexf / type;
  if(x > 0 && checkandswap(indexf, -1)) {
      return;
    }
  if(x < (type-1) && checkandswap(indexf, 1)) {
      return;
    }
  if(y > 0 && checkandswap(indexf, -type)) {
      return;
    }
  if(y < (type-1) && checkandswap(indexf, type)) {
      return;
    }
}

int Engine::count() const
{
  return levels.size();
}
