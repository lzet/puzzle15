#include "level.h"

Level::Level(const QString &resource): img(resource), path(resource)
{

}

const QPixmap &Level::fullImage() const
{
  return img;
}

const QString &Level::pathToImage() const
{
  return path;
}
