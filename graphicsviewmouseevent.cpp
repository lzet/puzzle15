#include "graphicsviewmouseevent.h"

GraphicsViewMouseEvent::GraphicsViewMouseEvent(QWidget *parent)
  :QGraphicsView(parent)
{
  sceneval = new QGraphicsScene();
}

GraphicsViewMouseEvent::~GraphicsViewMouseEvent()
{
  delete sceneval;
}

void GraphicsViewMouseEvent::loadItems(const Engine &engine)
{
  sceneval->setSceneRect(0, 0, engine.currentLevel().fullImage().width(), engine.currentLevel().fullImage().height());
  sceneval->clear();
  setScene(sceneval);
  picmatrix.clear();
  for(auto &f: engine.fields) {
      picmatrix[f.number()] = scene()->addPixmap(f.image());
    }
}

void GraphicsViewMouseEvent::addItem(const Field &f)
{
  picmatrix[f.number()] = scene()->addPixmap(f.image());

}

void GraphicsViewMouseEvent::showItems(const Engine &engine)
{
  int count = 0;
  for(auto &f: engine.fields) {
      int x = count%engine.type;
      int y = count/engine.type;
      picmatrix[f.number()]->setPos(x*f.width, y*f.height);
      count++;
    }
}

void GraphicsViewMouseEvent::mousePressEvent(QMouseEvent *event)
{
  for(auto i = picmatrix.begin(); i != picmatrix.end(); ++i){
      QPointF p = mapToScene(event->pos());
      if(p.x() >= i.value()->pos().x() && p.x() < (i.value()->pos().x()+i.value()->pixmap().width())
        && p.y() >= i.value()->pos().y() && p.y() < (i.value()->pos().y()+i.value()->pixmap().height()))
        emit mouseclick(i.key());
    }
}
