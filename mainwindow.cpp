#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow), engine()
{
  ui->setupUi(this);
  engine.loadlevels();
  selectLevel(0);
  gv = new GraphicsViewMouseEvent();
  gv->setMouseTracking(true);
  ui->tabWidget->addTab(gv, "Game");
  connect(gv, SIGNAL(mouseclick(int)), this, SLOT(onclick(int)));
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::selectLevel(int to)
{
  if(engine.hasLevels()) {
      engine += to;
      ui->frame->setStyleSheet("background-image: url("+engine.currentLevel().pathToImage()+"); background-position: center;");
    }
}

void MainWindow::showField()
{
  if(engine.fields.empty())
    return;
  gv->showItems(engine);
}

void MainWindow::onclick(int index)
{
  engine.clickto(index);
  showField();
  if(engine.check()) {
      ui->tabWidget->setCurrentIndex(0);
      QString txt = "You win!\nClick count: ";
      txt += QString::number(engine.stats, 10);
      QMessageBox msg(QMessageBox::Information, "Statistics", txt, QMessageBox::Ok, this);
      msg.exec();
    }
}

void MainWindow::on_nextBtn_clicked()
{
  selectLevel(+1);
}

void MainWindow::on_prevBtn_clicked()
{
  selectLevel(-1);
}

void MainWindow::on_startBtn_clicked()
{
  int type;
  if(ui->type33rBtn->isChecked())
    type = 3;
  else if(ui->type44rBtn->isChecked())
    type = 4;
  else
    type = 5;
  engine.startGame(type);
  ui->tabWidget->setCurrentIndex(1);
  gv->loadItems(engine);
  engine.shuffle();
  showField();
}

void MainWindow::on_addBtn_clicked()
{
  QString fileName = QFileDialog::getOpenFileName(this, "Open File","","PNG (*.png)");
  if(!fileName.isEmpty()) {
      if(!engine.loadlevel(fileName)) {
          QString txt = "Picture characteristic: \n"
                        "  format: PNG\n"
                        "  max size: 320px";
          QMessageBox msg(QMessageBox::Critical, "Import Error", txt, QMessageBox::Ok, this);
          msg.exec();
        }
      else {
          int sz = static_cast<int>(engine.count());
          selectLevel(sz);
        }
    }
}
