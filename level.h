#ifndef LEVEL_H
#define LEVEL_H
#include <QString>
#include <QPixmap>

class Level
{
  QPixmap img;
  QString path;
public:
  explicit Level(const QString &resource);
  const QPixmap& fullImage() const;
  const QString& pathToImage() const;
};

#endif // LEVEL_H
