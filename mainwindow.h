#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMouseEvent>
#include "engine.h"
#include "graphicsviewmouseevent.h"

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

private slots:
  void on_nextBtn_clicked();

  void on_prevBtn_clicked();

  void on_startBtn_clicked();

private:
  Ui::MainWindow *ui;
  Engine engine;
  GraphicsViewMouseEvent *gv;
  void selectLevel(int to);
  void showField();
private slots:
  void onclick(int index);
  void on_addBtn_clicked();
};

#endif // MAINWINDOW_H
