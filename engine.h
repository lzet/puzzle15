#ifndef ENGINE_H
#define ENGINE_H
#include <vector>
#include <field.h>
#include <level.h>
#define MAX_SIZE_PIC 320

class Engine
{
  long currentlevel;
  std::vector<Level> levels;
  bool checkandswap(int index, int indexdif);
  bool oddcheck() const;
public:
  int stats;
  int type;
  std::vector<Field> fields;
  Engine() = default;
  bool check() const;
  void loadlevels();
  bool loadlevel(const QString &fname);
  bool hasLevels() const;
  const Level& currentLevel() const;
  void operator+=(int to);
  void startGame(int type);
  void shuffle();
  void clickto(int index);
  int count() const;
};

#endif // ENGINE_H
