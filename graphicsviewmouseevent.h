#ifndef GRAPHICSVIEWMOUSEEVENT_H
#define GRAPHICSVIEWMOUSEEVENT_H
#include <QGraphicsView>
#include <QMouseEvent>
#include <QGraphicsItemGroup>
#include "engine.h"
#include "field.h"

class GraphicsViewMouseEvent : public QGraphicsView
{
  Q_OBJECT
  QGraphicsScene *sceneval;
  QMap<int, QGraphicsPixmapItem*> picmatrix;

public:
  explicit GraphicsViewMouseEvent(QWidget *parent = 0);
  ~GraphicsViewMouseEvent();
  void loadItems(const Engine &engine);
  void addItem(const Field &f);
  void showItems(const Engine &engine);
protected:
  void mousePressEvent(QMouseEvent *event) override;
signals:
  void mouseclick(int index);
};

#endif // GRAPHICSVIEWMOUSEEVENT_H
